import Calc from '../src/calc.js';

define("Calc", function(){
	it("Should add stuff together", function(){
		let c = new Calc();
		expect(c.add(4,5)).toBe(9);
	});
})
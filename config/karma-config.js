var webpack = require("webpack");

var webpackConfig = require("./webpack-config-test.js");

module.exports = function (config) {
	config.set({
		browsers: [ "PhantomJS" ],
		singleRun: false, //just run once by default
        autoWatch: true,
		frameworks: [ "jasmine" ], //use the jasmine test framework
		files: [
			"webpack-entry-test.js" //just load this file
		],
		plugins: [ 
			"karma-phantomjs-launcher",
			"karma-jasmine",
			"karma-sourcemap-loader",
			"karma-webpack",
			"karma-coverage"
		],
		preprocessors: {
			"webpack-entry-test.js": [ "webpack", "sourcemap", "coverage" ] //preprocess with webpack and our sourcemap loader
		},
		coverageReporter: {
			type: "lcovonly", //produces a html document after code is run
			dir: "../coverage/" //path to created html doc
		},
		reporters: [ "progress", "coverage" ], //report results in this format
		webpack: webpackConfig
	})
}
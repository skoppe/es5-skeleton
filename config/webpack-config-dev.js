var path = require("path");
var webpack = require('webpack');
var config = require('./webpack-config-base.js');

config.devtool = 'eval-source-map';
config.plugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development'),
  }),
];
config.entry = { app: ["./src/main.js"] };
config.output = {
    path: path.resolve(__dirname, "build"),
    publicPath: "/assets/",
    filename: "bundle.js"
  };

module.exports = config;
var webpack = require('webpack');
var config = require('./webpack-config-base.js');

config.cache = true;
config.devtool = "inline-source-map"; //just do inline source maps instead of the default
config.module.preLoaders = [
	{
		test: /\.js?$/,
		include: /src/,
		exclude: /(node_modules|bower_components)/,
		loader: 'babel-istanbul',
		query: {
			cacheDirectory: true,
		},
	},
	{
		test: /-spec\.js$/,
		include: /test/,
		exclude: /(bower_components|node_modules)/,
		loader: 'babel',
		query: {
			cacheDirectory: true,
		},
	},
];
config.webpackServer = {
	noInfo: true //please don"t spam the console when running in karma!
};
config.plugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development'),
  }),
];

module.exports = config;
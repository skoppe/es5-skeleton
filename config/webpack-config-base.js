module.exports = {
	module: {
		loaders: [
			{
				test: /\.js$/,
				include: /src/,
				exclude: /(bower_components|node_modules)/,
				loader: 'babel',
				query: {
					cacheDirectory: true,
				},
			},
		],
	},
};